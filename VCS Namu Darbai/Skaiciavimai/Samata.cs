﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skaiciavimai
{
    public class Samata
    {
        public static float Kainos(float num1, float num2)
        {
            return num1 * num2;
        }

        public static float BendraSamata(float num1, float num2, float num3)
        {
            return num1 + num2 + num3;
        }
    }
}
