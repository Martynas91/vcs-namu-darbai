﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skaiciavimai
{
    public class Geometrija
    {
        public static float Perimetras(float num1, float num2)
        {
            return (num1 + num2) * 2;
        }

        public static float SienuBeLangu(float num1, float num2)
        {
            return num1 * num2;
        }

        public static float SienosSuLangais(float num1, float num2)
        {
            return num1 - (num2 / 100);
        }
    }
}
