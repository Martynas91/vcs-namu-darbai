﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFai
{
    class Program
    {
        static void Main(string[] args)
        {
        pradzia:
            Console.WriteLine("Pasirinkite kalba parasydami: LT, EN arba RU");
            string kalba = Console.ReadLine();

            if (kalba == "LT")
            {
                Console.WriteLine("\nLabas!");
            }
            else if (kalba == "EN")
            {
                Console.WriteLine("\nHello!");
            }
            else if (kalba == "RU")
            {
                Console.WriteLine("\nPrivet!");
            }
            else
            {
                Console.WriteLine("\nBlogai pasirinkta kalba");
                goto pradzia;
            }

        pradzia2:
            Console.WriteLine(Environment.NewLine + "Parasyk savo varda");
            string vardas = Console.ReadLine();

            Console.WriteLine(Environment.NewLine + vardas + ", parasyk savo pavarde");
            string pavarde = Console.ReadLine();

            Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", kiek tau metu?");
            int amzius = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("\nO koks tavo ugis? (Pvz.: 1.56)");
            float ugis = Convert.ToSingle(Console.ReadLine());

            string KiekVaiku;
            string KiekAnuku;

        lytisKlausimas:
            Console.WriteLine("\nAr tu moteris?");
            string lytis = Console.ReadLine();

            if (lytis == "Taip" || lytis == "taip")
            {
                lytis = "moteris";
            }
            else if (lytis == "Ne" || lytis == "ne")
            {
                lytis = "vyras";
            }
            else
            {
                Console.WriteLine(Environment.NewLine + "Klaidingai atsakete i klausima apie lyti.");
                Console.WriteLine("Atsakykite Taip arba Ne");
                goto lytisKlausimas;
            }

            if (vardas != "" && pavarde != "" && amzius >= 0 && ugis >= 0.30)
            {
                if (amzius > 0 && amzius <= 10)
                {
                    Console.WriteLine("\nAr jau eini i mokykla?");
                    Console.WriteLine("Taip arba Ne");
                    string EinaMokykla = Console.ReadLine();

                    bool EinaMokyklaSalyga = EinaMokykla == "Taip";

                    if (EinaMokyklaSalyga)
                    {
                        Console.WriteLine("\nI kelinta klase eini?");
                        int klase = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Tu eini i mokykla, i " + klase + " klase");
                    }
                    else
                    {
                        Console.WriteLine("\nKiek menesiu liko laukti iki mokyklos?");
                        int menesiai = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Tu i mokykla eisi po " + menesiai + " menesiu");
                    }

                }

                else if (amzius > 10 && amzius <= 20)
                {
                    Console.WriteLine("\nAr jau baigei mokykla?");
                    Console.WriteLine("Taip arba Ne");
                    string BaigeMokykla = Console.ReadLine();

                    bool BaigeMokyklaSalyga = BaigeMokykla == "Taip";

                    if (BaigeMokyklaSalyga)
                    {
                        Console.WriteLine("\nAr jau istojai mokytis?");
                        Console.WriteLine("Taip arba Ne");
                        string ArIstojo = Console.ReadLine();

                        bool ArIstojoSalyga = ArIstojo == "Taip";

                        if (ArIstojoSalyga)
                        {
                            Console.WriteLine("\nKur istojai?");
                            string KurIstojo = Console.ReadLine();

                            Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                            Console.WriteLine("Tu baigei mokykla ir istojai i " + KurIstojo);
                        }

                        else
                        {
                            Console.WriteLine("\nKiek metu liko mokytis?");
                            double LikoMokytis = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                            Console.WriteLine("Mokykla baigsi po " + LikoMokytis + " metu");
                        }
                    }
                }
                else if (amzius > 20 && amzius <= 30)
                {
                    Console.WriteLine("\nAr gyveni Vilniuje?");
                    Console.WriteLine("Taip arba Ne");
                    string KurGyvena = Console.ReadLine();

                    bool GyvenaVilniuje = KurGyvena == "Taip";

                    if (GyvenaVilniuje)
                    {
                        Console.WriteLine("\nKuriame rajone?");
                        string Rajonas = Console.ReadLine();

                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Jus gyvenate Vilniuje, " + Rajonas + " rajone");
                    }
                    else
                    {
                        Console.WriteLine("\nO kuriame mieste?");
                        string Miestas = Console.ReadLine();

                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Jus gyvenate " + Miestas + " mieste");
                    }
                }
                else if (amzius > 30 && amzius <= 40)
                {
                    Console.WriteLine("\nAr turite vaiku?");
                    Console.WriteLine("Taip arba Ne");
                    string vaikai = Console.ReadLine();

                    bool VaikaiSalyga = vaikai == "Taip";

                    if (VaikaiSalyga)
                    {
                        Console.WriteLine("\nKiek turite vaiku?");
                        int VaikaiSkaicius = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Jus turite " + VaikaiSkaicius + " vaikus");
                    }
                    else
                    {
                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Jus neturite vaiku?");
                    }
                }
                else if (amzius > 40 && amzius <= 50)
                {
                    Console.WriteLine("\nKokia Jusu nuomone apie tos pacios lyties santuokas?");
                    Console.WriteLine("Teigama ar Neigiama?");
                    string Nuomone = Console.ReadLine();

                    bool NuomoneSalyga = Nuomone == "Teigiama";

                    if (NuomoneSalyga)
                    {
                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Jusu nuomone apie tos pacios lyties santuokas yra " + Nuomone);
                    }
                    else
                    {
                        Console.WriteLine("\nParasykite kodel?");
                        string Neigiama = Console.ReadLine();

                        Console.WriteLine("\nJusu nuomone apie tos pacies lyties santuokas yra neigiama, nes: " + Neigiama);
                    }
                }
                else if (amzius > 50 && amzius <= 70)
                {
                    Console.WriteLine("\nAr turite vaiku?");
                    Console.WriteLine("Taip arba Ne");
                    string TuriVaiku = Console.ReadLine();

                    Console.WriteLine("\nAr turite anuku?");
                    Console.WriteLine("Taip arba Ne");
                    string TuriAnuku = Console.ReadLine();

                    bool TuriVaikuSalyga = TuriVaiku == "Taip";
                    bool TuriAnukuSalyga = TuriAnuku == "Taip";

                    if (TuriVaikuSalyga)
                    {
                        Console.WriteLine("\nKiek turite vaiku?");
                        int VisoVaiku = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("\nKiek is ju berniuku?");
                        int Berniukai = Convert.ToInt32(Console.ReadLine());

                        int Mergaites = Atimtis(VisoVaiku, Berniukai);

                        KiekVaiku = "Jus turite " + VisoVaiku + " vaikus. Is ju " + Berniukai + " berniuku ir " + Mergaites + " mergaiciu";
                    }
                    else
                    {
                        KiekVaiku = "Jus neturite vaiku";
                    }
                    if (TuriAnukuSalyga)
                    {
                        Console.WriteLine("\nKiek turite anuku?");
                        int VisoAnuku = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("\nKiek is ju berniuku?");
                        int Berniukai = Convert.ToInt32(Console.ReadLine());

                        int Mergaites = Atimtis(VisoAnuku, Berniukai);

                        KiekAnuku = "Jus turite " + VisoAnuku + " anukus. Is ju " + Berniukai + " berniuku ir " + Mergaites + " mergaiciu";
                    }
                    else
                    {
                        KiekAnuku = "Jus neturite anuku";
                    }
                    Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                    Console.WriteLine(KiekVaiku);
                    Console.WriteLine(KiekAnuku);

                }
                else
                {
                    Console.WriteLine("\nAr sportuojate?");
                    Console.WriteLine("Taip arba Ne");
                    string sportas = Console.ReadLine();

                    bool SportasSalyga = sportas == "Taip";

                    if (SportasSalyga)
                    {
                        Console.WriteLine("\nKiek kartu per savaite?");
                        int KartaiPerSavaite = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Jus sportuojate " + KartaiPerSavaite + " kartus per savaite");
                    }
                    else
                    {
                        Console.WriteLine(Environment.NewLine + vardas + " " + pavarde + ", tau yra " + amzius + " metu, tavo ugis " + ugis + "m., tu esi " + lytis);
                        Console.WriteLine("Jus nesportuojate");
                    }
                }
            }
            else
            {
                Console.WriteLine(Environment.NewLine + "Blogai ivesti duomenys");
                goto pradzia2;
            }

            if (kalba == "LT")
            {
                Console.WriteLine("\nNorint isjungti programa iveskite ATE");
                string exit = Console.ReadLine();
                while (exit != "ATE")
                {
                    Console.WriteLine("\nNorint isjungti programa iveskite EXIT");
                    exit = Console.ReadLine();
                }
            }
            if (kalba == "EN")
            {
                Console.WriteLine("\nIf you want to close a program write BYE");
                string exit = Console.ReadLine();
                while (exit != "BYE")
                {
                    Console.WriteLine("\nIf you want to close a program write BYE");
                    exit = Console.ReadLine();
                }
            }
            if (kalba == "RU")
            {
                Console.WriteLine("\nShtobi otkliuchit programu, napishite DAVAI");
                string exit = Console.ReadLine();
                while (exit != "DAVAI")
                {
                    Console.WriteLine("\nShtobi otkliuchit programu, napishite DAVAI");
                    exit = Console.ReadLine();
                }
            }
        }

        static int Atimtis(int num1, int num2)
        {
            return num1 - num2;
        }
    }
}
