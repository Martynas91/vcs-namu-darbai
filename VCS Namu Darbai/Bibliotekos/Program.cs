﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skaiciavimai;

namespace Bibliotekos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Apskaiciuosime darbu samata kambariui isklijuoti tapetais");
            Console.WriteLine("\nIveskite kambario ploti metrais");
            float plotis = Convert.ToSingle(Console.ReadLine());

            Console.WriteLine("\nIveskite kambario ilgi metrais");
            float ilgis = Convert.ToSingle(Console.ReadLine());

            Console.WriteLine("\nIveskite kambario auksti metrais");
            float aukstis = Convert.ToSingle(Console.ReadLine());

            Console.WriteLine("\nIveskite kiek procentu uzima langai ir durys");
            float LangaiDurys = Convert.ToSingle(Console.ReadLine());

            float Perimetras = Geometrija.Perimetras(plotis, ilgis);
            float SienuPlotas = Geometrija.SienuBeLangu(Perimetras, aukstis);
            float SienuPlotasSuLangais = Geometrija.SienosSuLangais(SienuPlotas, LangaiDurys);

            Console.WriteLine("\nKambario sienu plotas yra: " + SienuPlotasSuLangais + " m2");

            Console.WriteLine("");
            Console.WriteLine("\nParasykite koks turi buti remontas: Greitas ar Kokybiskas");
            string remontas = Console.ReadLine();

            if (remontas == "Greitas")
            {
                float MeistroKaina = 20;
                float KlijuKaina = 1;
                float TapetuKaina = 10;

                float Meistras = Samata.Kainos(SienuPlotasSuLangais, MeistroKaina);
                float Klijai = Samata.Kainos(SienuPlotasSuLangais, KlijuKaina);
                float Tapetai = Samata.Kainos(SienuPlotasSuLangais, TapetuKaina);
                float samata = Samata.BendraSamata(Meistras, Klijai, Tapetai);

                Console.WriteLine("\nKambario isklijavimas tapetais kainuos: " + samata);
                Console.WriteLine("\nSamata susidaro is:");
                Console.WriteLine("Meistro darbas: " + Meistras);
                Console.WriteLine("Meistro darbas: " + Klijai);
                Console.WriteLine("Meistro darbas: " + Tapetai);
            }
            else if (remontas == "Kokybiskas")
            {
                float MeistroKaina = 40;
                float KlijuKaina = 2;
                float TapetuKaina = 10;

                float Meistras = Samata.Kainos(SienuPlotasSuLangais, MeistroKaina);
                float Klijai = Samata.Kainos(SienuPlotasSuLangais, KlijuKaina);
                float Tapetai = Samata.Kainos(SienuPlotasSuLangais, TapetuKaina);
                float samata = Samata.BendraSamata(Meistras, Klijai, Tapetai);

                Console.WriteLine("\nKambario isklijavimas tapetais kainuos: " + samata);
                Console.WriteLine("\nSamata susidaro is:");
                Console.WriteLine("Meistro darbas: " + Meistras);
                Console.WriteLine("Meistro darbas: " + Klijai);
                Console.WriteLine("Meistro darbas: " + Tapetai);
            }

            Console.ReadLine();
        }
    }
}
