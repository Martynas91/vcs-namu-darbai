﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace KryziukaiNuliukai
{
    public partial class Form1 : Form
    {
        bool turn = true;   //true = X ejimas; false = O ejimas;
        int turn_count = 0;
        int O = 0;
        int X = 0;
        int DRAW = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Kryziukai Nuliukai zaidimas", "About");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (turn)
            {
                b.Text = "X";
            }
            else
            {
                b.Text = "O";
            }
            turn = !turn;
            b.Enabled = false;
            turn_count++;
            CheckForWinner();
        }

        private void CheckForWinner()
        {
            bool winner = false;
            
            // Tikriname horizontaliai:

            if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
            {
                winner = true;
            }
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
            {
                winner = true;
            }
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
            {
                winner = true;
            }

            // Tikriname vertikaliai:

            else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
            {
                winner = true;
            }
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
            {
                winner = true;
            }
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
            {
                winner = true;
            }

            // Tikriname istrizai:

            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
            {
                winner = true;
            }
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!A3.Enabled))
            {
                winner = true;
            }

            // Kai turime laimetoja:

            if (winner)
            {
                dissableButtons();
                string laimejo = "";
                if (turn)
                {
                    laimejo = "O laimejo!";
                    O++;
                    O_Count.Text = Convert.ToString(O);
                    
                }
                else
                {
                    laimejo = "X laimejo!";
                    X++;
                    X_Count.Text = Convert.ToString(X);
                }

                MessageBox.Show(laimejo, "Valio!");
            }

            // LYGIOSIOS

            else
            {
                if (turn_count == 9)
                {
                    MessageBox.Show("Niekas nelaimejo!", "Lygu");
                    DRAW++;
                    Draw_Count.Text = Convert.ToString(DRAW);
                }
            }
        }

        private void dissableButtons()
        {
            foreach (Control c in Controls)
            {
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            turn_count = 0;

            foreach (Control c in Controls)
            {
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (b.Enabled)
            {
                if (turn)
                {
                    b.Text = "X";
                }
                else
                {
                    b.Text = "O";
                }
            }
        }

        private void button_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            X_Count.Text = Convert.ToString(0);
            O_Count.Text = Convert.ToString(0);
            Draw_Count.Text = Convert.ToString(0);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string Save = File.ReadAllText(@"C:\Users\MJ\Desktop", Encoding.UTF8);
        }
    }
}
