﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong
{
    public partial class Form1 : Form
    {
        public int speed_left = 4;
        public int speed_top = 4;
        public int points = 0;


        public Form1()
        {
            InitializeComponent();
            timer1.Enabled = true;
            Cursor.Hide();
            GameOver.Left = (PlayGround.Width / 2) - (GameOver.Width / 2);
            GameOver.Top = (PlayGround.Height / 2) - (GameOver.Height / 2);
            GameOver.Hide();

            this.FormBorderStyle = FormBorderStyle.None;
            this.TopMost = true;
            this.Bounds = Screen.PrimaryScreen.Bounds;

            Racket.Top = PlayGround.Bottom - (PlayGround.Bottom / 20);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Racket.Left = Cursor.Position.X - (Racket.Width / 2);
            ball.Left += speed_left;
            ball.Top += speed_top;

            if (ball.Bottom >= Racket.Top && ball.Bottom <= Racket.Bottom && ball.Left >= Racket.Left && ball.Right <= Racket.Right)
            {
                Random r = new Random();
                speed_top += 2;
                speed_left = r.Next(5, 20);
                speed_top = -speed_top;
                points++;
                Rezult_label.Text = points.ToString();

                PlayGround.BackColor = Color.FromArgb(r.Next(155, 255), r.Next(155, 255), r.Next(155, 255));
                ball.BackColor = Color.FromArgb(r.Next(0, 150), r.Next(0, 150), r.Next(0, 150));
                Racket.Width = r.Next(50, 250);
            }

            if (ball.Left <= PlayGround.Left)
            {
                speed_left = -speed_left;
            }

            else if (ball.Right >= PlayGround.Right)
            {
                speed_left = -speed_left;
            }

            else if (ball.Top <= PlayGround.Top)
            {
                speed_top = -speed_top;
            }
            else if (ball.Bottom >= PlayGround.Bottom)
            {
                timer1.Enabled = false;
                GameOver.Show();
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Application.Exit();
            }

            if (e.KeyCode == Keys.Enter && timer1.Enabled == false)
            {
                ball.Top = 50;
                ball.Left = 50;
                speed_left = 4;
                speed_top = 4;
                points = 0;
                Rezult_label.Text = "0";
                timer1.Enabled = true;
                GameOver.Hide();
                PlayGround.BackColor = Color.White;
                Racket.Width = 200;
                ball.BackColor = Color.Red;
            }
        }
    }
}
